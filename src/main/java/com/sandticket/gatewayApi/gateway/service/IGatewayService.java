package com.sandticket.gatewayApi.gateway.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;

public interface IGatewayService {
     String getAccessToken(String login,String passwword);
     String callApiTasks(Map<String,String> header,String title,String description,String createdAt,String dueAt,String lastUpdateDate,String taskStatus,String taskId) throws IOException;
}
