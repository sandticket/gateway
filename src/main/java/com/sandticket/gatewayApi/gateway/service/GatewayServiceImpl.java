package com.sandticket.gatewayApi.gateway.service;

import com.sandticket.gatewayApi.gateway.global.ParameterStringBuilder;
import okhttp3.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class GatewayServiceImpl implements IGatewayService {

    @Override
    public String getAccessToken(String login, String passwword) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders entete = new HttpHeaders();
        String urlAPI = "https://auth.cloud-iam.com/auth/realms/sandticket/protocol/openid-connect/token";
        entete.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.put("username", Collections.singletonList(login));
        map.put("password", Collections.singletonList(passwword));
        map.put("client_id", Collections.singletonList("login-app"));
        map.put("grant_type", Collections.singletonList("password"));
        HttpEntity<MultiValueMap<String, String>> requeteHttp;
        requeteHttp = new HttpEntity<MultiValueMap<String, String>>(map, entete);
        ResponseEntity<String> reponse = restTemplate.postForEntity(urlAPI, requeteHttp , String.class);
        System.out.println(reponse);
        if(reponse.getBody() != null)
            return reponse.getBody();
        return reponse.getStatusCode().toString();
    }

    @Override
    public String callApiTasks
            (Map<String,String> header,String title,
             String description,String createdAt,
             String dueAt,String lastUpdateDate,
             String taskStatus,String taskId)
            throws IOException
    {
        //add to environnment variable when running server
        String taskManagerUrl = System.getenv("TASK_MANAGER_URL");
        String taskManagerPort = System.getenv("TASK_MANAGER_PORT");
        String[] responseArray;
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = null;
        Request request = null;

        if(title != null)
            formBody = new FormBody.Builder().add("title",title).build();
        if(description != null)
            formBody = new FormBody.Builder().add("description",description).build();
        if(createdAt != null)
            formBody = new FormBody.Builder().add("createdAt",createdAt).build();
        if(dueAt != null )
            formBody = new FormBody.Builder().add("dueAt",dueAt).build();
        if(lastUpdateDate != null)
            formBody = new FormBody.Builder().add("lastUpdateDate",lastUpdateDate).build();
        if(taskStatus != null)
            formBody = new FormBody.Builder().add("taskStatus",taskStatus).build();
        if(taskId != null)
            formBody = new FormBody.Builder().add("taskId",taskId).build();

        if(formBody != null){
            request = new Request.Builder()
                    .url("http://" + taskManagerUrl + ":" + taskManagerPort + "/tasks")
                    .post(formBody)
                    .build();
        }
        if(formBody == null){
            request = new Request.Builder()
                    .url("http://" + taskManagerUrl + ":" + taskManagerPort + "/tasks")
                    .build();
        }

        Call call = client.newCall(request);
        Response response = call.execute();
        if(response.body() != null)
            return response.body().string();

        return Integer.toString(response.code());
    }


}
