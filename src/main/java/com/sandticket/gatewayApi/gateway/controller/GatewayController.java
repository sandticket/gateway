package com.sandticket.gatewayApi.gateway.controller;


import com.sandticket.gatewayApi.gateway.service.GatewayServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("")
public class GatewayController {
    @Autowired
    private GatewayServiceImpl gatewayService;

    public GatewayController(final GatewayServiceImpl gatewayService){
        this.gatewayService = gatewayService;
    }

    @GetMapping("/login")
    public String getAccessToken(@RequestParam String loginName,
                                 @RequestParam String password)
    {
        System.out.println("Login : "+loginName+ "mdp :"+password);
        return gatewayService.getAccessToken(loginName,password);
    }

    @GetMapping("/tasks")
    public String callApiTasks(@RequestHeader Map<String, String> headers,
                               @RequestParam(required = false)String title,
                               @RequestParam(required = false)String description,
                               @RequestParam(required = false)String createdAt,
                               @RequestParam(required = false)String dueAt,
                               @RequestParam(required = false)String lastUpdateDate,
                               @RequestParam(required = false)String taskStatus,
                               @RequestParam(required = false)String taskId)
            throws IOException {
        headers.forEach((key, value) -> {
            System.out.println(String.format("Header '%s' = %s", key, value));
        });
        return gatewayService.callApiTasks(headers,title,description,createdAt,dueAt,lastUpdateDate,taskStatus,taskId);
    }
}
