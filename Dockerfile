FROM gradle:5.2.0-jdk11 as builder
USER root
COPY . /home/gradle/project
WORKDIR /home/gradle/project
RUN gradle assemble
RUN ls /home/gradle/project/build/libs

FROM openjdk:11.0-jdk-slim
# Copy our static executable
WORKDIR /usr/src/
COPY --from=builder /home/gradle/project/build/libs/gs-spring-boot-0.1.0.jar gs-spring-boot-0.1.0.jar
RUN ls
EXPOSE 8500
ENTRYPOINT ["java","-jar", "gs-spring-boot-0.1.0.jar"]